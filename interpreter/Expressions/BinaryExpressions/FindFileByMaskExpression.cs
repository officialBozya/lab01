﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace interpreter.Expressions.BinaryExpressions
{
    class FindFileByMaskExpression : BinaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context)
        {
            string path = context.Memory[Op1.Solve(context)];
            string mask = context.Memory[Op2.Solve(context)];
            string[] dirs = Directory.GetFiles(path, mask, SearchOption.AllDirectories);
            if (dirs == null) { context.Output.Text += "Do not find.\r\n"; }
            foreach (string d in dirs)
            {
                context.Output.Text += d + "\r\n";
            }
            return null;
        }
    }
}
