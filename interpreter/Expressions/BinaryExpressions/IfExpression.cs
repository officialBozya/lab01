﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions.BinaryExpressions
{
    class IfExpression:BinaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context)
        {
            if (Convert.ToBoolean(context.Memory[Op1.Solve(context)])) 
            {
                Op2.Solve(context);
            }
            return null;
        }
    }
}
