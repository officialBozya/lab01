﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions.BinaryExpressions.MathExpressions
{
    class GreaterExpression: BinaryExpression
    {
        public override int Priority { get { return 2; } }
        public override dynamic Solve(Context context)
        {
            var op1 = Convert.ToInt32(context.Memory[Op1.Solve(context)]);
            var op2 = Convert.ToInt32(context.Memory[Op2.Solve(context)]);
            if (!context.Memory.Keys.Contains("buffer"))
            {
                context.Memory.Add("buffer", "");
            }
            context.Memory["buffer"] = Convert.ToString(op1 > op2);
            return "buffer";
        }
    }
}
