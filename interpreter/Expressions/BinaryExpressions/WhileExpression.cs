﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions.BinaryExpressions
{
    class WhileExpression:BinaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context)
        {
            while (Convert.ToBoolean(context.Memory[Op1.Solve(context)]))
            {
                Op2.Solve(context);
            }
            return null;
        }
    }
}
