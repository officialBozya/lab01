﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions
{
    class BlockExpression:IExpression
    {
        public BlockExpression(string _name)
        {
            name = _name;
        }
        private string name;
        public int Priority { get { return 1; } }
        public dynamic Solve(Context context) 
        {
            Interpreter interpreter = new Interpreter();
            interpreter.Execute(context.Blocks[name], context, name);
            return null;
        }
    }
}
