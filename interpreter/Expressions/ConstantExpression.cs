﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions
{
    class ConstantExpression: IExpression
    {
        public ConstantExpression(string _value) 
        {
            value = _value;
        }
        private string value;
        public int Priority { get { return 4; } }
        public dynamic Solve(Context context)
        {
            if (!context.Memory.Keys.Contains("buffer"))
            {
                context.Memory.Add("buffer", "");
            }
            context.Memory["buffer"] = value;
            return "buffer";
        }
    }
}
