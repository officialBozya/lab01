﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace interpreter.Expressions
{
    class Context
    {
        public Context(TextBox _output, TreeView _treeView)
        {
            Output = _output;
            TreeView = _treeView;
            Memory = new Dictionary<string, string>();
            Blocks = new Dictionary<string, string>();
            Brackets = new Dictionary<string, string>();
            Quotes = new Dictionary<string, string>();
        }
        public Dictionary<string, string> Quotes { get; private set; }
        public Dictionary<string, string> Brackets { get; private set; }
        public Dictionary<string, string> Blocks { get; private set; }
        public Dictionary<string, string> Memory { get; private set; }
        public TextBox Output { get; private set; }
        public TreeView TreeView { get; private set; }
    }
}
