﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace interpreter.Expressions.UnaryExpressions
{
    class BackupFileExpression : UnaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context)
        {
            string openedFilename = context.Memory[Op1.Solve(context)];

            if (File.Exists(openedFilename))
            {
                File.Copy(openedFilename, openedFilename.Insert(openedFilename.LastIndexOf('.'), " backup"), true);
                context.Output.Text += "Backup file was created.\r\n";
            }
            else
            {
                context.Output.Text += "File is not exists.\r\n";
            }
            return null;
        }
    }
}
