﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace interpreter.Expressions.UnaryExpressions
{
    class DeleteFileExpression : UnaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context)
        {
            string fileName = context.Memory[Op1.Solve(context)];

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
                context.Output.Text += "File " + fileName + " is deleted.\r\n";
            }
            else
            {
                context.Output.Text += "File is not exists.\r\n";
            }
            return null;
        }
    }
}
