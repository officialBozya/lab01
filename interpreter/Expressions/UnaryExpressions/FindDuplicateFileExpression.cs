﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace interpreter.Expressions.UnaryExpressions
{
    public class FileDetails
    {
        public string FileName { get; set; }
        public string FileHash { get; set; }
    }
    class FindDuplicateFileExpression : UnaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context)
        {
            string path = context.Memory[Op1.Solve(context)];

            //Get all files from given directory
            var fileLists = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
            int totalFiles = fileLists.Length;

            List<FileDetails> finalDetails = new List<FileDetails>();
            List<string> ToDelete = new List<string>();
            finalDetails.Clear();
            //loop through all the files by file hash code
            foreach (var item in fileLists)
            {
                using (var fs = new FileStream(item, FileMode.Open, FileAccess.Read))
                {
                    finalDetails.Add(new FileDetails()
                    {
                        FileName = item,
                        FileHash = BitConverter.ToString(SHA1.Create().ComputeHash(fs)),
                    });
                }
            }
            //group by file hash code
            var similarList = finalDetails.GroupBy(f => f.FileHash)
                .Select(g => new { FileHash = g.Key, Files = g.Select(z => z.FileName).ToList() });


            //keeping first item of each group as is and identify rest as duplicate files to delete
            ToDelete.AddRange(similarList.SelectMany(f => f.Files.Skip(1)).ToList());
            context.Output.Text += "Total duplicate files " + ToDelete.Count + "\r\n";
            foreach (string d in ToDelete)
            {
                context.Output.Text += d + "\r\n";
            }
            return null;
        }
    }
}
