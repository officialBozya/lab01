﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions.UnaryExpressions
{
    class PrintExpression: UnaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context) 
        {
            var op = Op1.Solve(context);
            context.Output.Text += context.Memory[op] + "\r\n";
            return null;
        }
    }
}
