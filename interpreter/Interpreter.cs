﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using interpreter.Expressions;
using interpreter.Expressions.UnaryExpressions;
using interpreter.Expressions.BinaryExpressions;
using interpreter.Expressions.BinaryExpressions.MathExpressions;

namespace interpreter
{
    class Interpreter
    {
        public void Execute(string text, Context context, string blockName = null)
        {
            text = FindQuotes(text, context);
            text = FindBrackets(text, context);
            text = FindBlocks(text, context);
            string[] lines = text.Split('\n');
            foreach (var line in lines)
            {
                List<IExpression> tokens = new List<IExpression>();
                treeNodes = new List<TreeNode>();
                string expression = line.Trim('\r', ' ');

                do
                {
                    IExpression token = Parse(ref expression, context);
                    tokens.Add(token);
                }
                while (!string.IsNullOrWhiteSpace(expression));

                var solver = CreateTree(tokens);

                if (blockName != null) 
                {
                    context.TreeView.BeginUpdate();
                    context.TreeView.Nodes[context.TreeView.Nodes.Count-1].Nodes[blockName].Nodes.Add(treeNode);
                    context.TreeView.EndUpdate();
                }
                else
                {
                    context.TreeView.BeginUpdate();
                    context.TreeView.Nodes.Add(treeNode);
                    context.TreeView.EndUpdate();
                }

                solver.Solve(context);
            }
        }
        private IExpression Parse(ref string text, Context context)
        {
            IExpression expression = null;
            var firstToken = text.Split(' ')[0];

            if (firstToken == "=")
            {
                expression = new AssignmentExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "+")
            {
                expression = new PlusExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "-")
            {
                expression = new MinusExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "*")
            {
                expression = new MultiplyExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "/")
            {
                expression = new DivisionExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "<")
            {
                expression = new LessExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == ">")
            {
                expression = new GreaterExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "==")
            {
                expression = new EqualsExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "!=")
            {
                expression = new NonEqualsExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "Print")
            {
                expression = new PrintExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "CopyFile")
            {
                expression = new CopyFileExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "FindFileByMask")
            {
                expression = new FindFileByMaskExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "MoveFile")
            {
                expression = new MoveFileExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "BackupFile")
            {
                expression = new BackupFileExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "CreateFile")
            {
                expression = new CreateFileExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "DeleteFile")
            {
                expression = new DeleteFileExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "FindDuplicateFile")
            {
                expression = new FindDuplicateFileExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "if")
            {
                expression = new IfExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken == "while")
            {
                expression = new WhileExpression();
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken.StartsWith("@"))
            {
                expression = new QuotesExpression(firstToken);
                treeNodes.Add(new TreeNode(context.Quotes[firstToken]));
            }
            else if (firstToken.StartsWith("#"))
            {
                expression = new BlockExpression(firstToken);
                treeNodes.Add(new TreeNode(firstToken) { Name = firstToken });
            }
            else if (firstToken.StartsWith("$"))
            {
                expression = new BracketsExpression(firstToken);
                treeNodes.Add(new TreeNode(firstToken) { Name = firstToken });
            }
            else if ((firstToken.StartsWith("-") && firstToken.TrimStart('-').All(_ => char.IsDigit(_))) || firstToken.All(_ => char.IsDigit(_)))
            {
                expression = new ConstantExpression(firstToken);
                treeNodes.Add(new TreeNode(firstToken));
            }
            else if (firstToken.All(_ => char.IsLetterOrDigit(_)))
            {
                expression = new VariableExpression(firstToken);
                if (text.Split(' ').Length > 1 && text.Split(' ')[1] == "=") 
                {
                    treeNodes.Add(new TreeNode(firstToken));
                }
                else
                    treeNodes.Add(new TreeNode(context.Memory[firstToken]));
            }
            else
            {
                throw new Exception($"Unknow expression {firstToken};");
            }

            text = text.Remove(0, firstToken.Length).Trim('\r', ' ');
            return expression;
        }
        private IExpression CreateTree(List<IExpression> expressions)
        {
            IExpression tree = null;
            TreeNode treeNod = null;

            foreach (var token in expressions)
            {
                tree = AddToTree(tree, token, ref treeNod, treeNodes[expressions.IndexOf(token)]);
            }
            treeNode = treeNod;
            return tree;
        }
        private IExpression AddToTree(IExpression tree, IExpression node, ref TreeNode treeNod, TreeNode nodeTree)
        {
            if (tree == null)
            {
                if (treeNod == null)
                {
                    treeNod = nodeTree;
                }
                else
                {
                    treeNod.Nodes.Add(nodeTree);
                }
                return node;
            }

            if (tree.Priority < node.Priority)
            {
                return CheckTypeAndAdd(tree, node, ref treeNod, nodeTree);
            }
            else if (tree.Priority == node.Priority)
            {
                var temp = nodeTree;
                nodeTree = treeNod;
                treeNod = temp;
                return CheckTypeAndAdd(node, tree, ref treeNod, nodeTree);
            }
            else
            {
                var temp = nodeTree;
                nodeTree = treeNod;
                treeNod = temp;
                return AddToTree(node, tree, ref treeNod, nodeTree);
            }

        }
        private IExpression CheckTypeAndAdd(IExpression tree, IExpression node, ref TreeNode treeNod, TreeNode nodeTree)
        {
            switch (tree)
            {
                case BinaryExpression ex2:
                    if (ex2.Op1 == null)
                    {
                        ex2.Op1 = AddToTree(ex2.Op2, node, ref treeNod, nodeTree);
                    }
                    else
                    {
                        if (treeNod.Nodes.Count > 1)
                        {
                            TreeNode tmp = treeNod.Nodes[1];
                            ex2.Op2 = AddToTree(ex2.Op2, node, ref tmp, nodeTree);
                            treeNod.Nodes[1] = tmp;
                        }
                        else
                            ex2.Op2 = AddToTree(ex2.Op2, node, ref treeNod, nodeTree);
                    }
                    return ex2;

                case UnaryExpression ex1:
                    ex1.Op1 = AddToTree(ex1.Op1, node, ref treeNod, nodeTree);
                    return ex1;

                default:
                    var temp = nodeTree;
                    nodeTree = treeNod;
                    treeNod = temp;
                    return AddToTree(node, tree, ref nodeTree, treeNod);
            }
        }
        private string FindBlocks(string input, Context context) 
        {
            while (input.IndexOf("{") != -1) 
            {
                input = FindAndDelBlocks(input, 0, context);
            }
            return input;
        }
        private string FindAndDelBlocks(string input, int pos, Context context) 
        {
            int open = input.IndexOf("{", pos);
            int nextOpen = input.IndexOf("{", pos + 1);
            int close = input.IndexOf("}", pos);
            if (nextOpen > close || nextOpen < 0) 
            {
                string key = "#" + context.Blocks.Count;
                context.Blocks.Add(key ,input.Substring(open + 1, close - open - 2).Trim('\n', '\r', ' '));
                string left = input.Remove(open);
                string right = input.Substring(close + 1);
                string output = left.Trim('\n', '\r', ' ') + " " + key + "\r\n" + right.Trim('\n', '\r', ' ');
                return output;
            }
            else 
            {
                return FindAndDelBlocks(input, nextOpen, context);
            }
        }
        private string FindBrackets(string input, Context context)
        {
            while (input.IndexOf("(") != -1)
            {
                input = FindAndDelBrackets(input, 0, context);
            }
            return input;
        }
        private string FindAndDelBrackets(string input, int pos, Context context)
        {
            int open = input.IndexOf("(", pos);
            int nextOpen = input.IndexOf("(", pos + 1);
            int close = input.IndexOf(")", pos);
            if (nextOpen > close || nextOpen < 0)
            {
                string key = "$" + context.Brackets.Count;
                context.Brackets.Add(key, input.Substring(open + 1, close - open - 2).Trim('\n', '\r', ' '));
                string left = input.Remove(open);
                string right = input.Substring(close + 1);
                string output = left.Trim('\n', '\r', ' ') + " " + key + "\r\n" + right.Trim('\n', '\r', ' ');
                return output;
            }
            else
            {
                return FindAndDelBrackets(input, nextOpen, context);
            }
        }
        private string FindQuotes(string input, Context context)
        {
            int pos = 0;
            int open;
            int close;
            while (input.IndexOf("\"") != -1)
            {
                pos = open = input.IndexOf("\"", pos);
                close = input.IndexOf("\"", pos + 1);
                if (open < close)
                {
                    string key = "@" + context.Quotes.Count;
                    context.Quotes.Add(key, input.Substring(open + 1, close - open - 1).Trim('\n', '\r', ' '));
                    string left = input.Remove(open);
                    string right = input.Substring(close + 1);
                    input = left.Trim('\n', '\r', ' ') + " " + key + "\r\n" + right.Trim('\n', '\r', ' ');
                }
            }
            return input;
        }
        private List<TreeNode> treeNodes;
        private TreeNode treeNode;
    }
}
